.PHONY: clean All

All:
	@echo "----------Building project:[ bnsl - Debug ]----------"
	@cd "bnsl" && "$(MAKE)" -f  "bnsl.mk"
clean:
	@echo "----------Cleaning project:[ bnsl - Debug ]----------"
	@cd "bnsl" && "$(MAKE)" -f  "bnsl.mk" clean
