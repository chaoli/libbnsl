#ifndef UTILITY_H_
#define UTILITY_H_

#include <vector>
#include <cstdint>
#include <iostream>

using namespace std;

namespace bnsl
{

/// Real number (define an abstract floating point type REAL to provide a trade-off between precision and memory usage)
typedef double Real;



template<class container_type>
void print(container_type array)
{
	for(auto i = 0; i< array.size() ; ++i)
		std::cout << array[i] << ' ';
	std::cout<<endl;
}

/// Variable ID in Bayesian network. (should be a unsigned integer)
//	typedef uint8_t Variable; // Output面倒い


/** Type for associating weights with nodes */
//typedef std::vector<size_t> Weights;
}

#endif // UTILITY_H_
