#include <iostream>
#include <string>
#include <io.h>

#include "sparse_set.h"
#include "utility.h"
#include "adtree.h"
#include <vector>

#include "utility.h"

using namespace std;

using namespace bnsl;

void getSufStats(unique_ptr<ContabNode>& CTN, vector<size_t>& leafCount)
{

    cout << "CTN count is : " << CTN->COUNT << endl;

    if(0 == CTN->children.size() /*check if ContabNode is leaf*/) {
        leafCount.push_back(CTN->COUNT);
        return;
    } else {

        for(unique_ptr<ContabNode>& child : CTN->children) {

            if(child) {
                getSufStats(child, leafCount);
            }
        }
    }
}

Real loglikelihood(ADTree* adTree, size_t X_i, SparseSet<size_t> PA_i)
{

    SparseSet<size_t> firstVariableSet(X_i);
    SparseSet<size_t> FA_i = PA_i | firstVariableSet;
    unique_ptr<ContabNode> root1 = adTree->MakeContab(FA_i);

    vector<size_t> N_ij;
    // N_ij.push_back(77);

    getSufStats(root1, N_ij);

    print(N_ij);

    // unique_ptr<ContabNode> root2 = adTree->MakeContab(PA_i);
    return -101.1;
}

int main()
{
    cout << "BNSL main starts" << endl;

    // 文件名
    // string dataset_filename = "/Users/chaoli/bitbucket/libbnsl/datasets/figure_1_2_6.txt";
     string dataset_filename = "/Users/chaoli/bitbucket/libbnsl/datasets/figure_4.txt";

    // string dataset_filename = "/Users/wentingmen/bitbucket/libbnsl/datasets/figure_1_2_6.txt";
    //string dataset_filename = "/Users/wentingmen/bitbucket/libbnsl/datasets/figure_4.txt";

    // 空树
    ADTree adTree(5);

    // 初始化AD树的数据集
    adTree.ReadDataSetFile(dataset_filename);
    adTree.BuildADTree();

    size_t X_i = 0; // X_i = a3
    SparseSet<size_t> X_0(0);
    SparseSet<size_t> X_1(1);
    SparseSet<size_t> PA_i = X_0 | X_1; // PA_i = {a1, a2}
    Real score = loglikelihood(&adTree, X_i, PA_i);

    // cout << score << endl;

    cout << "BNSL main ends" << endl;
}
