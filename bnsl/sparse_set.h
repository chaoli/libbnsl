#ifndef SPARSE_SET_H_
#define SPARSE_SET_H_


#include <vector>
#include <iostream>
#include <sstream>
namespace bnsl
{
/**
* @file		sparse_set.h
* @class	SparseSet
* @brief	Adapts a std::vector<T> to provide a Set class. Brief description.
*
* Detailed description starts here.
*
* @author	chao li
* @date	2016/03/21
*/
template < class T,
         class Container = std::vector<T> >
class SparseSet
{
public:
	typedef typename Container::value_type value_type;
	typedef typename Container::size_type size_type;

	typedef typename Container::reference reference;
	typedef typename Container::const_reference const_reference;

	typedef typename Container::iterator iterator;
	typedef typename Container::const_iterator const_iterator;

	typedef Container container_type;
private:
	Container c;
public:
	SparseSet() : c() {}
	SparseSet( const T &t ) : c {t} {  }
	SparseSet( const SparseSet& other ) :c(other.c) {}
	SparseSet& operator= ( const SparseSet& other ) {
		if (this != &other) {
			this->c = other.c;
			this->c.shrink_to_fit();
		}
		return *this;
	}
	~SparseSet() {}

	///	@name	Set Operations
	///	@{

	/**
	 * @brief	Set Difference.
	 *
	 * Because C++ dose not allow the operator "\" (e.g. A \\ B), we use the alternative symbol "-" (e.g. A - B).
	 * @param other	other set
	 * @return		result set = this set \\ other set
	 */
	SparseSet operator- (const SparseSet & other) const {
		SparseSet result;
		std::set_difference( c.cbegin(), c.cend(), other.c.cbegin(), other.c.cend(), inserter( result.c, result.c.begin() ) );
		return result;
	}

	/**
	* @brief	Set Union.
	*
	* Because C++ dose not allow the operator "\" (e.g. A \\ B), we use the alternative symbol "-" (e.g. A - B).
	* @param other	other set
	* @return		result set = this set \\ other set
	*/
	SparseSet operator| (const SparseSet & other) const {
		SparseSet result;
		std::set_union( c.cbegin(), c.cend(), other.c.cbegin(), other.c.cend(), inserter( result.c, result.c.begin() ) );
		return result;
	}
	/**
	* @brief	Set Intersection.
	*
	* Because C++ dose not allow the operator "\" (e.g. A \\ B), we use the alternative symbol "-" (e.g. A - B).
	* @param other	other set
	* @return		result set = this set \\ other set
	*/
	SparseSet operator& (const SparseSet & other) const {
		SparseSet result;
		std::set_intersection( c.cbegin(), c.cend(), other.c.cbegin(), other.c.cend(), inserter( result.c, result.c.begin() ) );
		std::cout << result.c.capacity() << std::endl;
		return result;
	}
	///@}

	///	@name	Element access
	///	@{
	reference       operator[]( size_type pos ) {
		return c[pos];
	}
	const_reference operator[]( size_type pos ) const {
		return c[pos];
	}
	///	@}



	///	@name	Capacity
	///	@{
	bool empty() const {
		return c.empty();
	}
	size_type size() const {
		return c.size();
	}
	size_type capacity() const {
		return c.capacity();
	}
	///	@}


	/// \name Input/Output
	//@{
	/// output stream
	friend std::ostream& operator << ( std::ostream& os, const SparseSet& V ) {
		os << "{ ";
		for(auto i = 0; i< V.size() ; ++i)
			os << V[i] << " ";
		os << "}";
		return os;
	}
	//@}


	int add(int a, int b);

	//int add(int a, int b);///< 随时删除它，测试用

};

template < class T,
         class Container>
int SparseSet<T,Container>::add(int a, int b)
{
	return a+b;
}
}
#endif // SPARSE_SET_H_
