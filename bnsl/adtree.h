#ifndef ADTREE_H_
#define ADTREE_H_

#include <vector>
#include <memory>
#include <string>
#include <fstream>
#include <iterator>
#include <sstream>
#include <memory>
#include <iostream>
#include <cstddef>

#include "utility.h"
#include "sparse_set.h"
using std::vector;
using std::unique_ptr;
using std::string;
using namespace std;

namespace bnsl
{

typedef vector<size_t> Nums;

struct VaryNode;
/// ADNode : count and children
struct ADNode {
	size_t COUNT; ///< Brief description after the member
	vector< unique_ptr<VaryNode> > children; ///< Brief description after the member
	Nums leafList;



	static int ID ;
	const int currentID;


	ADNode(): children(), currentID(ID++) {	}



	//~ADNode() = default;
};
int ADNode::ID = 0;



/// VaryNode : mcv and children
struct VaryNode {
	size_t MCV;
	vector< unique_ptr<ADNode> > children;


	static int ID ;
	const int currentID;


	VaryNode(): children(), currentID(ID++) {	}
	//~VaryNode() = default;
};

int VaryNode::ID = 0;


struct ContabNode {
	size_t COUNT;
	vector< unique_ptr<ContabNode> > children;

	static int ID ;
	const int currentID;


	ContabNode(): children(), currentID(ID++) {	}



	// they are assumed to both be rooted at the same variable
	void subtract(unique_ptr<ContabNode> & other) {

		if(0 == children.size()/*check if ContabNode is leaf*/) {



			cout << "this ID "<<currentID << " COUNT " <<COUNT << " - other ID" << other->currentID << " COUNT " << other->COUNT << endl;

			COUNT -= other->COUNT;
			return;
		} else {
			for(size_t k = 0; k < children.size(); ++k) {
				if(children[k] == nullptr || other->children[k] == nullptr) {
					continue;
				}
				children[k]->subtract( other->children[k] );
			}
		}
	}

};
int ContabNode::ID = 0;

/**
 * @file	adtree.h
 * @class	ADTree
 * @brief	Brief description.
 *
 * Detailed description starts here.
 * @author	chao li
 * @date	2016/03/13
 */

class ADTree
{
public:
	typedef vector<size_t> Record;
private:
	vector<size_t> Arity;
	vector<Record> dataset;	///< clear dataset after build the tree, because we only need sufficient statistics.
	size_t M; ///< Number of attributes
	size_t R; ///< Number of records
	unique_ptr<ADNode> root_;
	size_t Rmin;
public:
	/*, root_(nullptr)*/

	ADTree(size_t r = 0)  : Arity(), Rmin(r) {}
	~ADTree() {}



	///@name after reading data set, start to build ADTree.
	//@{

	/**
	 * @brief 从硬盘上读取数据集
	 * @param filename the data set file name
	 */
	void 	ReadDataSetFile (std::string& filename);

	/// start to build the entire tree.

	void BuildADTree();

	unique_ptr<ContabNode> MakeContab(SparseSet<size_t> & variables) {
		return MakeContab(variables, this->root_);
	}
private:
	/**
	 * @brief Make a new ADnode called ADN.
	 * @param a_i			子树ADNode的根
	 * @param RecordNums		第二个参数。
	 * @return				ADnode的指针。
	 */
	unique_ptr<ADNode> MakeADTree(size_t a_i, Nums& RecordNums);


	/**
	 * @brief Make a new Vary node
	 * @param a_i abc
	 * @param RecordNums def
	 * @return
	 */
	unique_ptr<VaryNode> MakeVaryNode(size_t a_i, Nums& RecordNums);

	/**
	 * @brief	Brief description.
	 * @param variables
	 * @param ADN
	 * @return
	 */
	unique_ptr<ContabNode> MakeContab(SparseSet<size_t> & variables, unique_ptr< ADNode> & ADN);

	unique_ptr<ContabNode> makeContabLeafList(SparseSet<size_t> & variables, Nums & RecordNums);

	//@}
};

//////////////////////////////////////////////////////////////////////

unique_ptr<ContabNode> ADTree::makeContabLeafList(SparseSet<size_t> & variables, Nums & RecordNums)
{
	if (variables.size() == 0) {
		unique_ptr<ContabNode> CT(new ContabNode);
		CT -> COUNT = RecordNums.size();
		return CT;
	}

	int firstVariable = variables[0]; // first
	int cardinality = Arity[firstVariable];


	vector<Nums> Childnums;
	for(size_t k = 0; k < cardinality; ++k) {
		Childnums.push_back(Nums());
	}

	for(size_t j : RecordNums) {
		size_t k = dataset[j][firstVariable];
		Childnums[k].push_back(j);
	}


	unique_ptr<ContabNode> CT(new ContabNode());

	SparseSet<size_t> firstVariableSet(firstVariable);
	SparseSet<size_t> remainings = variables - firstVariableSet;




	for (int k = 0; k < cardinality; k++) {
		if ( Childnums[k].size() > 0) {
			unique_ptr<ContabNode> CT_k = makeContabLeafList(remainings, Childnums[k]);
			CT -> children.push_back(std::move (CT_k) );
		} else {
			CT -> children.push_back(nullptr);
		}
	}

	return CT;
}

unique_ptr<ContabNode> ADTree::MakeContab(SparseSet<size_t> & variables, unique_ptr< ADNode> & ADN)
{
	cout << "makeContab and variables are : " << endl;
	print(variables);
	cout << "ADN ID " << ADN ->currentID << endl;
	cout << "ADN child size is : " << ADN -> children.size()<< endl;
	cout << "ADN Count is " <<ADN->COUNT<< endl;


	// The base case
	if (0 == variables.size()) {
		unique_ptr<ContabNode> CT(new ContabNode);
		CT->COUNT = ADN->COUNT;

		cout << "                        CCCCCCCCCCCCCCCCCCCCCCC" << CT ->currentID << endl;
		cout << "CT ID " << CT ->currentID << endl;
		cout << "CT COUNT " << CT ->COUNT << endl;
		cout << "leaf count: " << CT->COUNT << endl;

		return CT;
	} else {

		unique_ptr< VaryNode> & VN = ADN -> children[  variables[0] /*current variable*/ - ( M - ADN -> children.size() /*start variable*/)  ];
		size_t MCV =  VN -> MCV ;


		cout << "                        VVVVVVVVVVVVVVVVVVVV" << VN ->currentID << endl;
		cout << "VN ID " << VN ->currentID << endl;


		SparseSet<size_t> firstVariableSet(variables[0]);
		SparseSet<size_t> remainings = variables - firstVariableSet;


		unique_ptr<ContabNode> CT(new ContabNode);
		CT->COUNT= ADN->COUNT;

		cout << "                        CCCCCCCCCCCCCCCCCCCCCCC" << CT ->currentID << endl;
		cout << "CT ID " << CT ->currentID << endl;
		cout << "CT COUNT " << CT ->COUNT << endl;

		unique_ptr<ContabNode> CT_MCV = MakeContab(remainings, ADN);



		for (size_t k = 0; k < Arity[variables[0]]; ++k) {

			//cout << "make " << k << "th CTN" << endl;

			if  (k == MCV || nullptr == VN->children[k])  { // check k == MCV or NULL (count = 0)
				CT->children.push_back(nullptr);

			} else {
				unique_ptr<ADNode> & ADN_k = VN->children[k];
				// CT_k = MakeContab(a2...an, ADN_k)
				unique_ptr<ContabNode> CT_K(nullptr);



				cout << "make CT k using ADN k " << endl;
				cout << "VN ID" << VN->currentID << endl;
				cout << "                        VVVVVVVVVVVVVVVVVVVV" << ADN_k ->currentID << endl;

				if (0 == ADN_k->leafList.size() /*check has leafList*/ ) {
					CT_K = MakeContab(remainings, ADN_k);
				} else {
					CT_K = makeContabLeafList(remainings, ADN_k->leafList);
				}

				CT->children.push_back(std::move (CT_K) ) ;

				CT_MCV->subtract(CT->children[k]);


			}

		}
		CT->children[VN->MCV] = std::move ( CT_MCV ) ;
		return std::move (CT );
	}
}




/**
 * @brief 详细说明。
 */
void 	ADTree::ReadDataSetFile (std::string& filename)
{
	string line = "测试ADTree::ReadDataSetFile， 不应该出现";
	// open the dataset file
	std::ifstream f (filename);

	// process header
	getline(f, line);

	// 给string加一层包装类，可以将string转换为整数数组
	stringstream lineStream(line);

	size_t cardinality; // Arity[m]
	while (lineStream >> cardinality) {
		Arity.push_back(cardinality);
	}


	//process data
	while(getline(f, line)) {

		// 给string加一层包装类，可以将string转换为整数数组
		stringstream lineStream(line);

		ADTree::Record record;	// dataset[r]
		size_t data_value;	// record[m]

		// get one record.
		while (lineStream >> data_value) {
			record.push_back(data_value);
		}

		// put one record to the dataset.
		dataset.push_back(record);
	}

	this->R = dataset.size();
	this->M = Arity.size();
}

/**
 * @brief To build the entire tree, we must call MakeADTree(a<sub>1</sub> = 0, RecordNums = {1...R})
 * Assuming binary atributes, the cost of building a tree from R records and M attributes is bounded above by
		\f{eqnarray*}{
				\sum_{k=0}^{log_{2}R} \frac{R}{2^k} {M\choose k}
		\f}
 */
void ADTree::BuildADTree()
{
	// MakeADTree的第（1/2）个参数。
	const size_t a_1 = 0;
	// MakeADTree的第（2/2）个参数。
	vector<size_t> RecordNums;
	for(size_t index = 0; index < R; ++index) {
		RecordNums.push_back(index);
	}

	// start building
	root_ = MakeADTree(a_1, RecordNums);
	// end building and clear dataset, since we have calculated sufficient statistic.
}

/**
 * Detailed description starts here.
 */
unique_ptr<ADNode> ADTree::MakeADTree(size_t a_i, Nums& RecordNums)
{
	unique_ptr<ADNode> ADN (new ADNode);
	//unique_ptr<VaryNode> VN(new VaryNode);



	// 初始化count
	ADN->COUNT = RecordNums.size();


	//cout << "ADN ID " << ADN ->currentID << endl;
	//cout << ADN -> COUNT<<endl<<endl;


	// Check if we should use a leaf list
	if (ADN->COUNT < Rmin) {
		ADN->leafList = RecordNums;
		return std::move(ADN);
	}

	// 接下来，造儿子。一共有 ｛ai, ai+1, ..., M}
	for(size_t a_j = a_i; a_j < M; ++a_j) {
		ADN->children.push_back( MakeVaryNode(a_j, RecordNums) );

		//cout << "AD ID"<<ADN ->currentID << endl;
		//cout << "VN ID"<<ADN ->children[ a_j - a_i ]->currentID << endl;
		//cout << ADN->children[a_j - a_i]->children.size() << endl;
		//cout << "I" << endl;
	}



	return ADN ;
}

/**
 * @brief Make a new VaryNode called ADN.
 * @param a_i abcd
 * @param RecordNums efsg
 * @return
 */


unique_ptr<VaryNode> ADTree::MakeVaryNode(size_t a_i, Nums& RecordNums)
{



	//Make a new VaryNode called VN.
	unique_ptr<VaryNode> VN(new VaryNode);


	vector<Nums> Childnums;
	for(size_t k = 0; k < ADTree::Arity[a_i]; ++k) {
		Childnums.push_back(Nums());
	}

	for(size_t j : RecordNums) {
		size_t v_i_j = dataset[j][a_i];
		Childnums[v_i_j].push_back(j);
	}

	size_t max = 0;
	size_t mcv = 0;

	for(size_t k = 0; k < Arity[a_i]; ++k) {
		Nums& nums = Childnums[k];
		if(nums.size() > max) {
			max = nums.size();
			mcv = k;
		}

	}

	VN -> MCV = mcv;

	//cout << "VN ID " << VN ->currentID << endl;
	//cout << "Vary on a" << a_i << endl;
	//cout << "number of child : " << Arity[a_i] << endl;
	//cout << "MCV of VaryNode" << a_i << "is :" << VN -> MCV  << endl << endl;

	for(size_t k = 0; k < Arity[a_i]; ++k) {

		// ADN->children.push_back( MakeVaryNode(a_j, RecordNums) );



		if( (0 == Childnums[k].size()) || ( k == VN -> MCV )) {
			VN -> children.push_back (nullptr);
		} else {
			VN -> children.push_back (MakeADTree(a_i+1, Childnums[k]) );
		}
	}
	return VN;
}

}
#endif // ADTREE_H_
