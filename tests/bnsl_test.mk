##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=bnsl_test
ConfigurationName      :=Debug
WorkspacePath          := "/Users/wentingmen/bitbucket/libbnsl"
ProjectPath            := "/Users/wentingmen/bitbucket/libbnsl/tests"
IntermediateDirectory  :=Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=wenting men
Date                   :=07/04/2016
CodeLitePath           :="/Users/wentingmen/Library/Application Support/codelite"
LinkerName             :=/usr/bin/llvm-g++
SharedObjectLinkerName :=/usr/bin/llvm-g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="bnsl_test.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)$(UNIT_TEST_PP_SRC_DIR)/src $(IncludeSwitch). $(IncludeSwitch)/usr/local/Cellar/unittest-cpp/1.5.1/include/UnitTest++/UnitTest++ $(IncludeSwitch)/Users/chaoli/bitbucket/libbnsl/bnsl $(IncludeSwitch)/Users/wentingmen/bitbucket/libbnsl/bnsl 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)UnitTest++ 
ArLibs                 :=  "libUnitTest++.a" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)$(UNIT_TEST_PP_SRC_DIR)/Debug $(LibraryPathSwitch)/usr/local/Cellar/unittest-cpp/1.5.1/lib 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/llvm-ar rcu
CXX      := /usr/bin/llvm-g++
CC       := /usr/bin/llvm-gcc
CXXFLAGS :=  -g -std=c++14 -std=c++11 $(Preprocessors)
CFLAGS   :=  -g $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/llvm-as


##
## User defined environment variables
##
CodeLiteDir:=/Applications/codelite.app/Contents/SharedSupport/
Objects0=$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/adtree_test.cpp$(ObjectSuffix) $(IntermediateDirectory)/SparseSetTest.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d Debug || $(MakeDirCommand) Debug


$(IntermediateDirectory)/.d:
	@test -d Debug || $(MakeDirCommand) Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/wentingmen/bitbucket/libbnsl/tests/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM "main.cpp"

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) "main.cpp"

$(IntermediateDirectory)/adtree_test.cpp$(ObjectSuffix): adtree_test.cpp $(IntermediateDirectory)/adtree_test.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/wentingmen/bitbucket/libbnsl/tests/adtree_test.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/adtree_test.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/adtree_test.cpp$(DependSuffix): adtree_test.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/adtree_test.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/adtree_test.cpp$(DependSuffix) -MM "adtree_test.cpp"

$(IntermediateDirectory)/adtree_test.cpp$(PreprocessSuffix): adtree_test.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/adtree_test.cpp$(PreprocessSuffix) "adtree_test.cpp"

$(IntermediateDirectory)/SparseSetTest.cpp$(ObjectSuffix): SparseSetTest.cpp $(IntermediateDirectory)/SparseSetTest.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/wentingmen/bitbucket/libbnsl/tests/SparseSetTest.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/SparseSetTest.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/SparseSetTest.cpp$(DependSuffix): SparseSetTest.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/SparseSetTest.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/SparseSetTest.cpp$(DependSuffix) -MM "SparseSetTest.cpp"

$(IntermediateDirectory)/SparseSetTest.cpp$(PreprocessSuffix): SparseSetTest.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/SparseSetTest.cpp$(PreprocessSuffix) "SparseSetTest.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r Debug/


