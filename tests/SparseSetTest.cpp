#include <UnitTest++.h>

#include <sparse_set.h>

using bnsl::SparseSet;

SUITE(SparseSetTest)
{
	class SparseSetFixture
	{
	public:
		SparseSet<size_t> X1;
		SparseSet<size_t> X2;
		SparseSet<size_t> X3;
		SparseSet<size_t> X4;
		SparseSet<size_t> X5;

		SparseSet<size_t> X;
		SparseSet<size_t> Y;
		SparseSet<size_t> Z;


		SparseSetFixture() : X1(1),X2(2),X3(3),X4(4),X5(5) {

			X = X4|X2|X3;	// X = {2,3,4}
			Y = X1|X5|X3;	// Y = {1,3,5}
		}
	};

	/// set Union, difference, intersection
	TEST_FIXTURE(SparseSetFixture, SetOperations) {
		// Union
		Z = X | Y; //	Z = {1,2,3,4,5}
		CHECK_EQUAL(Z.capacity() , 5 );
		for(auto i = 0; i< Z.capacity() ; ++i) {
			CHECK_EQUAL(Z[i], i+1);
		}
        // intersection
		Z = X & Y; // Z = {3}
		CHECK_EQUAL(1, Z.capacity()  );
		CHECK_EQUAL(Z[0] , 3 );
        // set difference
		Z = X - Y; 	// Z = {2, 4}
		CHECK_EQUAL(Z.capacity() , 2 );
		CHECK_EQUAL(Z[0] , 2 );
		CHECK_EQUAL(Z[1] , 4 );

		CHECK_EQUAL(X.size(), X.capacity());
		CHECK_EQUAL(Y.size(), Y.capacity());
		CHECK_EQUAL(Z.size(), Z.capacity());
	}

	/// constructor
	TEST_FIXTURE(SparseSetFixture, constructor) {

		// default
		SparseSet<size_t> U;
		CHECK_EQUAL(U.size(), U.capacity());

		// single
		SparseSet<size_t> V(99);
		CHECK_EQUAL(V.size(), V.capacity());
		CHECK_EQUAL(V[0], 99);

		//copy
		SparseSet<size_t> copyV(V);
		CHECK_EQUAL(copyV.size(), copyV.capacity());
		CHECK_EQUAL(copyV[0], 99);

		Z = X | Y; //	Z = {1,2,3,4,5}
		SparseSet<size_t> copyZ(Z);
		CHECK_EQUAL(copyZ.size(), copyZ.capacity());
		CHECK_EQUAL(copyZ.capacity() , 5 );
		for(auto i = 0; i< copyZ.capacity() ; ++i) {
			CHECK_EQUAL(copyZ[i], i+1);
		}
		// operator=()
		V = U;	//	U = {}, V = {99}
		CHECK_EQUAL(0, V.capacity());

		U = Z; // U = {}, Z = {1,2,3,4,5}
		CHECK_EQUAL(U.capacity() , 5 );
		for(auto i = 0; i< U.capacity() ; ++i) {
			CHECK_EQUAL(U[i], i+1);
		}

		CHECK_EQUAL(X.size(), X.capacity());
		CHECK_EQUAL(Y.size(), Y.capacity());
		CHECK_EQUAL(Z.size(), Z.capacity());
	}
}
