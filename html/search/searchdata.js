var indexSectionsWithContent =
{
  0: "abcdegilmnoprsuv~",
  1: "acsv",
  2: "b",
  3: "abcimsu",
  4: "abceglmoprsv~",
  5: "acdilmr",
  6: "cinrsv",
  7: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "related"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Friends"
};

